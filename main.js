const http = new slhttp();

const postsHolder = document.querySelector("#posts");
document.querySelector("#getData").addEventListener('click', getData);

function getData(){
    http.get("https://jsonplaceholder.typicode.com/posts")
        // .then(data => console.log(data))
        .then(data => data.forEach((post) => addPostAccordianItem(post)))
        .catch(err => console.warn(err));
    const value = {
        title: 'JavaScript',
        body: 'Welcome to the API Consuming',
        userId: 1
    };
    // http.post("https://jsonplaceholder.typicode.com/posts", value)
    //     .then(data => console.log(data))
    //     .then(data => data.forEach((post) => addPostAccordianItem(post)))
    //     .catch(err => console.warn(err));
    // http.put("https://jsonplaceholder.typicode.com/posts/10", value)
    //     .then(data => console.log(data))
    //     .then(data => data.forEach((post) => addPostAccordianItem(post)))
    //     .catch(err => console.warn(err));
    http.delete("https://jsonplaceholder.typicode.com/posts/10")
        // .then(data => console.log(data))
        // .then(data => data.forEach((post) => addPostAccordianItem(post)))
        .catch(err => console.warn(err));
}

function addPostAccordianItem(postData){
    let outerDiv = document.createElement('div');
    let h2Element = document.createElement('h2');
    let innerDiv = document.createElement('div');
    let cardBody = document.createElement('p');

    outerDiv.className = 'card mb-2';
    h2Element.className = 'card-title';
    h2Element.innerHTML = `# ${postData.id}  ${postData.title}`;

    innerDiv.className = 'card-body';
    cardBody.className = 'card-text';
    cardBody.innerHTML = postData.body;
    innerDiv.appendChild(h2Element);
    innerDiv.appendChild(cardBody);
    outerDiv.appendChild(innerDiv);
    postsHolder.appendChild(outerDiv);
}